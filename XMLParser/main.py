from XMLParser.XmlParse import BookList
from XMLParser.LogDec import name_info
from WordCounter.OptionValidator import OptionValidator

@name_info
def main():
    a=BookList()
    input_file = 'Books.xml'
    output_file = 'titles.txt'
    OptionValidator(input_file).validate()
    books = BookList()
    dane = books.xml_parser_tuple(input_file)
    a.write_to_file(dane, output_file)


if __name__ == '__main__':
    main()