import xml.dom.minidom
import logging
import sys
from XMLParser.LogDec import name_info



class BookList:
    def __init__(self):
        self.author = ''
        self.title = ''
        self.genre = ''
        self.price = ''
        self.publ_date = ''
        self.description = ''

    @name_info
    def xml_parser_tuple(self, filename):
        try:
            DOMTree = xml.dom.minidom.parse(filename)
            collection = DOMTree.documentElement
            books = collection.getElementsByTagName("book")
            books_list = []

            for book in books:
                if book.hasAttribute("id"):
                    id = book.getAttribute("id")

                self.author = book.getElementsByTagName('author')[0]
                t_author = self.author.childNodes[0].data
                self.title = book.getElementsByTagName('title')[0]
                t_title = self.title.childNodes[0].data
                self.genre = book.getElementsByTagName('genre')[0]
                t_genre = self.genre.childNodes[0].data
                self.price = book.getElementsByTagName('price')[0]
                t_price = self.price.childNodes[0].data
                self.publ_date = book.getElementsByTagName('publish_date')[0]
                t_publish_date = self.publ_date.childNodes[0].data
                self.description = book.getElementsByTagName('description')[0]
                t_description = self.description.childNodes[0].data

                books_list.append(("Id: " + id, "author: " + t_author, "title: " + t_title, "genre: " + t_genre,
                                   "price: " + t_price, "publish date: " + t_publish_date,
                                   "description: " + t_description))
            for t in books_list:
                print(t)
            return books_list
        except IOError:
            print("brak pliku")
            sys.exit(0)

    @name_info
    def write_to_file(self,dane, save_file):
        try:
            output_file = open(save_file, 'w')
            for d in dane:
                output_file.write(str(d) + "\n\n")
            return save_file
        except IOError:
            logging.info('Nie utworzono pliku')
            print('Nie utworzono pliku')


