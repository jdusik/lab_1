import argparse
from WordCounter.LogDec import  name_info


@name_info
def parseArg():
    parser = argparse.ArgumentParser(description='Act like Unix wc', prog='WordCount', epilog='Results in wc.dat file.')
    parser.add_argument('filename', type=str, action='append')
    parser.add_argument('-w', action="store_true", help='count words')
    parser.add_argument('-l', action="store_true", help='count lines')
    parser.add_argument('-c', action="store_true", help='count bits')
    parser.print_help()
    args = parser.parse_args()
    return args
