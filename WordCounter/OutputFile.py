import os
from WordCounter.LogDec import  name_info


class OutputFile():
    def __init__(self, filename):
        self.filename = filename

    @name_info
    def removeFileIfAlreadyExist(self):
        path = os.path.realpath(self.filename)
        if os.path.isfile(path):
            os.unlink(self.filename)

    @name_info
    def saveResultsToFile(self, results):
        self.removeFileIfAlreadyExist()
        output_file = open(self.filename, 'w')
        output_file.write('%s \t %s' % (results,self.filename))
        output_file.close()
