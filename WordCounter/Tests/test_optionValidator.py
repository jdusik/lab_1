from unittest import TestCase
from unittest.mock import patch

from WordCounter import OptionValidator

class TestOptionValidator(TestCase):
    def test_should_raiseNotAStingException(self):
        input_filename = 123
        validator = OptionValidator.OptionValidator(input_filename)
        self.assertRaises(OptionValidator.NotAString, validator.validate)

    def test_should_raise_No_SuchFile_Exception(self):
        input_filename = "/path/to/not/existing/file.txt"
        validator = OptionValidator.OptionValidator(input_filename)
        self.assertRaises(OptionValidator.NotSuchFile, validator.validate)

    @patch('os.path.isfile', return_value=True)
    def test_should_correctly_validate_file(self, mock):
        input_filename = "file_exist.txt"
        validator = OptionValidator.OptionValidator(input_filename)
        validator.validate()
