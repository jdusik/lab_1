from unittest import TestCase
from unittest.mock import patch
from WordCounter import OutputFile


class TestOutputFile(TestCase):


    def test_should_save_results_in_file(self):
        output_file='test.txt'
        results=[1,2,3]
        save=OutputFile.OutputFile(output_file)
        save.saveResultsToFile(results)

    def test_should_remove_existing_file(self):
        output_file='test.txt'
        save=OutputFile.OutputFile(output_file)
        save.removeFileIfAlreadyExist()      