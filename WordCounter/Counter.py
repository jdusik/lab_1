from WordCounter.AbstractCounter import AbstractCounter
from WordCounter.LogDec import name_info


class Counter(AbstractCounter):
    def __init__(self, file):
        self.file = file
        self.lineNumber = 0
        self.wordNumber = 0
        self.Number = 0

    @name_info
    def countLines(self, filename):
        for line in filename:
            self.lineNumber += 1
        return self.lineNumber

    @name_info
    def countWords(self, filename):
        for line in filename:
            self.wordNumber += len(line.split())
        return self.wordNumber

    @name_info
    def countBytes(self, filename):
        for line in filename:
            self.Number += len(line)
        return self.Number

    @name_info
    def countAll(self, filename):
        for line in filename:
            self.lineNumber += 1
            self.wordNumber += len(line.split())
            self.Number += len(line)
        return self.lineNumber, self.wordNumber, self.Number

    @name_info
    def choose_option(self, option):
        if option.c:
            return self.countBytes(self.file)
        elif option.l:
            return self.countLines(self.file)
        elif option.w:
            return self.countWords(self.file)
        else:
            return self.countAll(self.file)
