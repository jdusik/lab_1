import os.path
from WordCounter.AbstractOptionValidator import AbstractValidator
from WordCounter.LogDec import  name_info

class NotSuchFile(Exception):
    pass


class NotAString(Exception):
    pass


class OptionValidator(AbstractValidator):
    def __init__(self, filename):
        self.filename = filename

    @name_info
    def validate(self):
        if not self._isString():
            raise NotAString()
        if not self._file_exist():
            raise NotSuchFile()

    def _isString(self):
        return isinstance(self.filename, str)

    def _file_exist(self):
        return os.path.isfile(self.filename)

