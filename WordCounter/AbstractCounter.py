import abc


class AbstractCounter(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def countLines(self, filename):
        pass

    @abc.abstractmethod
    def countWords(self, filename):
        pass

    @abc.abstractmethod
    def countBytes(self, filename):
        pass
