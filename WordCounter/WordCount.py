from __future__ import absolute_import
from functools import reduce
from WordCounter.ArgumentParser import parseArg
from WordCounter.OptionValidator import OptionValidator
from WordCounter.Counter import Counter
from WordCounter.OutputFile import OutputFile
import operator
from WordCounter.LogDec import  name_info


@name_info
def main():
    arguments = parseArg()
    filename = reduce(operator.add, arguments.filename)
    OptionValidator(filename).validate()
    input_file = open(filename, 'r')
    #print(arguments)
    count = Counter(input_file)
    wc = count.choose_option(arguments)
    print(wc)
    input_file.close()
    output_file = 'wc.dat'
    OutputFile(output_file).saveResultsToFile(wc)


if __name__ == "__main__":
    main()
